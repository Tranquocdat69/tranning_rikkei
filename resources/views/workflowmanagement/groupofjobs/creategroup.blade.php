@extends('master')
@section('content')
<div class="content" style="margin: 1% 0 20% 3%;">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header"><strong>Create group of jobs</strong></div>
                    <div class="card-body">
                        <div id="error" style="display: none">{{session('error')}}</div>
                        <form method="POST" action="" enctype="multipart/form-data" class="form-horizontal">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Name <small>(*)</small></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    <div style="color: red;">
                                        @if($errors->has('name'))
                                            {{ $errors->first('name') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 text-md-right form-control-label">Avatar</label>

                                <div class="col-md-6">
                                    <input id="name" type="file" name="avatar" value="" accept=".jpeg,.jpg,.png,.gif">
                                    <div style="color: red;">
                                        @if($errors->has('avatar'))
                                            {{ $errors->first('avatar') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0" style="margin-left: 30%;padding-top: 2% !important;">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-sm btn-info">
                                        Create
                                    </button>
                                    <a href="{{ route('group_of_jobs') }}" class="btn btn-sm btn-danger" style="margin-left: 9%;">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
@stop
