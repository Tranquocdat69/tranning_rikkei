@extends('master')
@section('content')
<div class="content" style="margin: 1% 0 20% 3%;">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><strong>Edit group of jobs</strong></div>
                    <div class="card-body">
                        <div id="error" style="display: none">{{session('error')}}</div>
                        <form method="POST" action="{{ route('edit_group',['id'=>$id]) }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name <small>(*)</small></label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $group->name }}">
                                    <div style="color: red;">
                                        @if($errors->has('name'))
                                            {{ $errors->first('name') }}
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Avatar</label>

                                <div class="col-md-6">
                                    <input id="name" type="file" name="avatar" value="" accept=".jpeg,.jpg,.png,.gif">
                                    <div style="color: red;">
                                        @if($errors->has('avatar'))
                                            {{ $errors->first('avatar') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if($group->avatar)
                            <img src="/img/avatar/{{ $group->avatar }}" style="margin-left: 35%;margin-bottom: 2%;" height="90px" width="100px" />
                            @endif

                            <div class="form-group row mb-0" style="margin-left: 30%;padding-top: 4% !important;">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-sm btn-info">
                                        Update
                                    </button>
                                    <a href="{{ route('group_of_jobs') }}" class="btn btn-sm btn-danger" style="margin-left: 9%;">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@endsection
@section('css')
    <link rel="stylesheet" href="">
@stop
@section('js')
@stop
