<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FiWork Managements</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/w2ui-1.5.rc1.min.css') }}">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
    @yield('css')
</head>
<body>
    <div class="content" style="margin: 0 0 30% 0;">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-8 offset-md-2">
                    <div class="card">
                        <div class="card-header text-center">
                            <strong>Change Password</strong>
                        </div>
                        <div class="card-body card-block">
                            @if (session('error'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Error!</strong> {{session('error')}}
                            </div>
                            @endif 
                            <form method="POST" action="{{ route('change_pass_after_reset') }}">
                                @csrf
                                <div class="has-success form-group">
                                    <input type="text" hidden class="form-control" name="email">
                                </div>

                                <div class="has-success form-group">
                                    <label for="inputSuccess2i" class=" form-control-label">Old password <small>(*)</small></label>
                                    <input id="name" type="password" class="form-control" name="old_password" value="{{old('old_password')}}">
                                    <div style="color: red;">
                                        @if($errors->has('old_password'))
                                        {{ $errors->first('old_password') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-warning form-group">
                                    <label for="inputWarning2i" class=" form-control-label">New Password <small>(*)</small></label>
                                    <input id="name" type="password" class="form-control" name="password" value="{{ old('password') }}">
                                    <div style="color: red;">
                                        @if($errors->has('password'))
                                        {{ $errors->first('password') }}
                                        @endif
                                    </div>
                                </div>

                                <div class="has-danger has-feedback form-group">
                                    <label for="inputError2i" class=" form-control-label">Confirm password <small>(*)</small></label>
                                    <input id="name" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
                                    <div style="color: red;">
                                        @if($errors->has('confirm_password'))
                                        {{ $errors->first('confirm_password') }}
                                        @endif
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success">
                                        Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- .row -->
        </div><!-- .animated -->
    </div><!-- .content -->
</body>
</html>