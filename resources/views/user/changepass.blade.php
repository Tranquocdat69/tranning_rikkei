@extends('master')
@section('content')
<div class="content" style="margin: 0 0 30% 0;">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-8 offset-md-2">
                <div class="card">
                    <div class="card-header text-center">
                        <strong>Change Password</strong>
                    </div>
                    <div class="card-body card-block">
                        @if (session('error'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Error!</strong> {{session('error')}}
                        </div>
                        @endif
                        <form method="POST" action="{{ route('change_pass') }}">
                            @csrf
                            <div class="has-success form-group">
                                <label for="inputSuccess2i" class=" form-control-label">Old password <small>(*)</small></label>
                                <input id="name" type="password" class="form-control" name="old_password" value="{{old('old_password')}}">
                                <div style="color: red;">
                                    @if($errors->has('old_password'))
                                    {{ $errors->first('old_password') }}
                                    @endif
                                </div>
                            </div>

                            <div class="has-warning form-group">
                                <label for="inputWarning2i" class=" form-control-label">New Password <small>(*)</small></label>
                                <input id="name" type="password" class="form-control" name="password" value="{{ old('password') }}">
                                <div style="color: red;">
                                    @if($errors->has('password'))
                                    {{ $errors->first('password') }}
                                    @endif
                                </div>
                            </div>

                            <div class="has-danger has-feedback form-group">
                                <label for="inputError2i" class=" form-control-label">Confirm password <small>(*)</small></label>
                                <input id="name" type="password" class="form-control" name="confirm_password" value="{{ old('confirm_password') }}">
                                <div style="color: red;">
                                    @if($errors->has('confirm_password'))
                                    {{ $errors->first('confirm_password') }}
                                    @endif
                                </div>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('profile') }}" class="btn btn-outline-danger">Cancel</a>
                                &nbsp;
                                <button type="submit" class="btn btn-success">
                                    Update
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->
@stop()