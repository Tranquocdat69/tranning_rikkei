@extends('master')
@section('content')
<div class="content" style="margin: 0 0 40% 0">
    <div class="animated fadeIn">
        <div class="buttons">
            <div class="row">
                @foreach($wallets as $w)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-five">
                                <div class="stat-icon dib flat-color-1">
                                    <img src="{{asset('images/wallet')}}/{{$w->image_name}}" height="50">
                                </div>
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-text"><span class="count">{{number_format($w->amount_of_money)}}</span> VNĐ</div>
                                        <div class="stat-heading">
                                            <b>{{$w->name}}</b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div><!-- .row -->
        </div> <!-- .buttons -->
    </div><!-- .animated -->
</div>
@stop