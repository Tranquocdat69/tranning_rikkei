@extends('master')
@section('content')
<div class="content" style="margin: 0 0 10% 0">
    <div class="animated fadeIn">
        <div class="buttons">
            <div class="card">
                <div class="card-body">
                    <h4 class="text-center"><b>Charts of {{$wallet[0]->name}}
                            from
                            {{date("d/m/Y", strtotime($start_date))}}
                            to
                            {{date("d/m/Y", strtotime($end_date))}}</b>
                    </h4>
                    <div style="margin-left: 87%; margin-top: -28px">
                        <button type="button" class="btn btn-large btn-success" data-toggle="modal" data-target="#staticModal">Custom Date</button>
                        <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" style="display: none;" aria-hidden="true">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticModalLabel"><b>Custom date</b></h5>
                                    </div>
                                    <div class="modal-body" style="margin-left: 43px;margin-top: -10px">
                                        <form action="{{ route('search-chart',['wallet_id'=>$wallet_id]) }}" method="GET" id="myForm">
                                            <div class="row">
                                                <div class="form-group has-success">
                                                    <label for="datepicker_from" class="control-label mb-1">Start Date</label>
                                                    <input type="text" id="datepicker_from" name="start_date" class="form-control cc-name valid" placeholder="Choose start date">
                                                    <div style="color: red">
                                                        @if($errors->has('start_date'))
                                                        {{$errors->first('start_date')}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group has-success">
                                                    <label for="datepicker_to" class="control-label mb-1">End Date</label>
                                                    <input type="text" id="datepicker_to" name="end_date" class="form-control cc-name valid" placeholder="Choose end date">
                                                    <div style="color: red">
                                                        @if($errors->has('end_date'))
                                                        {{$errors->first('end_date')}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
                                                &nbsp;
                                                <button type="submit" id="submit" class="btn btn-success" disabled="disabled">
                                                    Search
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div>
                            </div>
                            <div>
                                {{ $chart_expense->container() }}
                                {{ $chart_expense->script() }}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <div>
                                {{ $chart_income->container() }}
                                {{ $chart_income->script() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- .buttons -->
    </div><!-- .animated -->
</div>
<script>
    $(function() {
        $('#datepicker_from').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#datepicker_to').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#datepicker_from').keypress(function(d) {
            d.preventDefault();
        });
        $('#datepicker_to').keypress(function(d) {
            d.preventDefault();
        });
        $('#myForm input[type="text"]').on('input change', function() {
            if ($('#datepicker_from').datepicker('getDate') != null && $('#datepicker_to').datepicker('getDate') != null) {
                if ($('#datepicker_from').val() < $('#datepicker_to').val()) {
                    $('#submit').prop('disabled', false);
                }
                if ($('#datepicker_from').val() > $('#datepicker_to').val()) {
                    $('#submit').prop('disabled', true);
                }
                if ($('#datepicker_from').val() == $('#datepicker_to').val()) {
                    $('#submit').prop('disabled', true);
                }
            } else {
                $('#submit').prop('disabled', true);
            }
        });
    });
</script>
@stop