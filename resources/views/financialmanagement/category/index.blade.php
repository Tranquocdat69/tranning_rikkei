@extends('master')
@section('content')
<div class="content" style="margin: 0 0 40% 0">
    <a href="{{route('add-category',['wallet_id'=>$wallet_selected[0]->id, 'status' => $status])}}" class="btn btn-success pull-right"><b>Add category</b></a>
    <div class="col-md-4">
        <button type="button" class="btn btn-outline-success mb-1" title="{{$wallet_selected[0]->name}}" data-toggle="modal" data-target="#smallmodal">
            <img src="{{asset('images/wallet')}}/{{$wallet_selected[0]->image_name}}" class="img-responsive" height="50">
            &nbsp;&nbsp;
            <b>{{$wallet_selected[0]->name}}</b>
        </button>
        <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <div class="row">
                                <div class="col-md-12">
                                    @foreach($wallets as $w)
                                    <div class="{{ ($wallet_id == $w->id) ? 'selected_wallet' : '' }}">
                                        <div class="stat-widget-four change_wallet" data-value="{{$w->id}}">
                                            <div class="stat-icon dib">
                                                <img src="{{asset('images/wallet')}}/{{$w->image_name}}" height="50">
                                            </div>
                                            <div class="stat-content">
                                                <div class="text-left dib">
                                                    <div class="stat-heading">{{$w->name}}</div>
                                                    <div class="stat-text">
                                                        {{number_format($w->amount_of_money)}} <b>VNĐ</b>

                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div><!-- .row -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="animated fadeIn">
        <div class="buttons">
            <!-- Left Block -->
            <div class="row" style="margin-top: -70px">
                <div class="col-md-8 offset-md-3">
                    <div class="card">
                        <div class="card-header">
                            <strong>Categories</strong>
                        </div>
                        <div class="card-body">
                            <!-- begin -->
                            <div class="offset-md-4">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active show" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Expense</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Income</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <?php Helpers::showAllCategories($wallet_id, $categories_expense) ?>
                                </div>
                                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                    <?php Helpers::showAllCategories($wallet_id, $categories_income) ?>
                                </div>
                            </div>
                            <!-- end -->
                        </div><!-- /# card -->
                    </div>
                </div><!-- .row -->
            </div> <!-- .buttons -->
        </div><!-- .animated -->
    </div>
</div>
<script>
    $(".change_wallet").click(function() {
        var temp = $(this).data("value");
        var url = window.location.href.slice(0, window.location.href.lastIndexOf('/'));
        $(location).attr('href', url + '/' + temp);
    });
</script>
@stop