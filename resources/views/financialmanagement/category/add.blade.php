@extends('master')
@section('content')
<div class="content" style="margin: 0 0 30% 0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Create new category</strong>
                    </div>
                    <div class="card-body">
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="text-center">Add Category</h3>
                                </div>
                                <hr>
                                <form action="" id="formCreateWallet" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input name="wallet_id" hidden type="text" class="form-control" value="{{$wallet_id}}">
                                    </div>
                                    <div class="form-group offset-md-4">
                                        <div class="form-check-inline form-check">
                                            <label for="inline-radio1" class="form-check-label ">
                                                <input type="radio" id="inline-radio1" name="status" class="form-check-input" {{$checked_income}} value="1">Income
                                            </label>
                                            &nbsp; &nbsp; &nbsp; &nbsp;
                                            <label for="inline-radio2" class="form-check-label ">
                                                <input type="radio" id="inline-radio2" name="status" class="form-check-input" {{$checked_expense}} value="0">Expense
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Category name</label>
                                        <input name="name" type="text" class="form-control" value="{{old('name')}}">
                                    </div>
                                    <div style="color: red">
                                        @if($errors->has('name'))
                                        {{$errors->first('name')}}
                                        @endif
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="cc-name" class="control-label mb-1">Parent Category</label>
                                        <select name="parent_id" id="select" class="form-control">
                                            <option value="0">Parent Category</option>
                                            <?php Helpers::getParentCategory($categories) ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="cc-number" class="control-label mb-1">Category avatar</label>&nbsp;&nbsp;
                                        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#staticModal">
                                            <input type="text" id="image_id" hidden name="image_id" value="1">
                                            <img id="preview_image" src="{{asset('images/wallet/1.png')}}" height="50">
                                        </button>
                                        <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="staticModalLabel">Choose avatar</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        @foreach($images as $i)
                                                        <img data-value="{{$i->id}}" class="select_image" src="{{asset('images/wallet')}}/{{$i->name}}" height="50">
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input name="created_at" hidden type="text" class="form-control" value="{{$date}}">
                                    </div>
                                    <div class="form-group">
                                        <input name="updated_at" hidden type="text" class="form-control" value="{{$date}}">
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('category',['wallet_id'=>$wallet_id]) }}" class="btn btn-outline-danger">Cancel</a>
                                        &nbsp;
                                        <button type="submit" class="btn btn-success">
                                            Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".select_image").click(function() {
        var src = $(this).attr("src");
        $("#preview_image").attr('src', src);
        $("#image_id").val($(this).data("value"));
        $("#staticModal").trigger("click");
    });

    $(".form-check-input").click(function() {
        var temp = $(this).val();
        var url = window.location.href.slice(0, window.location.href.lastIndexOf('/'));
        $(location).attr('href', url + '/' + temp);
    })
</script>
@stop()