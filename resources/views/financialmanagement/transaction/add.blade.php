@extends('master')
@section('content')
<div class="content" style="margin: 0 0 30% 0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Create transaction</strong>
                    </div>
                    <div class="card-body">
                        <!-- Credit Card -->
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="text-center">Add transaction</h3>
                                </div>
                                <hr>
                                <form action="#" method="post" id="formAddTransaction">
                                    @csrf
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="cc-exp" class="control-label mb-1">Wallet</label>
                                                <input id="cc-exp" hidden name="wallet_id_transaction" type="number" class="form-control cc-exp" value="{{$wallet_selected[0]->id}}">
                                                <button type="button" class="btn btn-outline-success mb-1" title="{{$wallet_selected[0]->name}}" data-toggle="modal" data-target="#smallmodal" style="width: 300px">
                                                    <img style="margin-left: -60px" src="{{asset('images/wallet')}}/{{$wallet_selected[0]->image_name}}" class="img-responsive" height="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <b>{{ $wallet_selected[0]->name }}</b>
                                                </button>
                                                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" style="display: none;" aria-hidden="true">
                                                    <div class="modal-dialog modal-sm" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        @foreach($wallets as $w)
                                                                        <div class="{{ ($wallet_id == $w->id) ? 'selected_wallet' : '' }}">
                                                                            <div class="stat-widget-four change_wallet" data-value="{{$w->id}}">
                                                                                <div class="stat-icon dib">
                                                                                    <img src="{{asset('images/wallet')}}/{{$w->image_name}}" height="50">
                                                                                </div>
                                                                                <div class="stat-content">
                                                                                    <div class="text-left dib">
                                                                                        <div class="stat-heading">{{$w->name}}</div>
                                                                                        <div class="stat-text">
                                                                                            {{number_format($w->amount_of_money)}} <b>VNĐ</b>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                            </div>
                                                                        </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div><!-- .row -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="categoryId" class="control-label mb-1">Category</label>
                                                <input id="categoryId" name="category_id" hidden type="number" class="form-control cc-exp ">
                                                <button type="button" class="btn btn-outline-success mb-1" data-toggle="modal" data-target="#mediumModal" style="width: 300px;">
                                                    <img style="margin-left: -60px" src="{{asset('images')}}/favicon.png" class="img-responsive" id="imageCategory" height="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <b id="nameCategory">Select Category</b>
                                                </button>
                                                <div style="color: red">
                                                    @if($errors->has('category_id'))
                                                    {{$errors->first('category_id')}}
                                                    @endif
                                                </div>
                                                <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="mediumModalLabel"><b>Select Category</b></h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <!-- begin -->
                                                                                <div class="offset-md-4">
                                                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                                                        <li class="nav-item">
                                                                                            <a class="nav-link active show" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Expense</a>
                                                                                        </li>
                                                                                        <li class="nav-item">
                                                                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Income</a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                                <div class="tab-content" id="pills-tabContent">
                                                                                    <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                                        <?php Helpers::showAllCategoriesTransaction($wallet_id, $categories_expense) ?>
                                                                                    </div>
                                                                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                                                        <?php Helpers::showAllCategoriesTransaction($wallet_id, $categories_income) ?>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end -->
                                                                            </div><!-- /# card -->
                                                                        </div>
                                                                    </div><!-- .row -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="money" class="control-label mb-1">Amount</label>
                                                <input id="money" name="money" type="text" class="form-control">
                                                <div style="color: red">
                                                    @if($errors->has('money'))
                                                    {{$errors->first('money')}}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group has-success">
                                                <label for="datepicker" class="control-label mb-1">Date</label>
                                                <input id="datepicker" name="created_at" class="form-control cc-name valid">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="cc-name" class="control-label mb-1">Note</label>
                                        <input id="cc-name" name="note" type="text" class="form-control cc-name valid">
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('transactions',['wallet_id'=>$wallet_id]) }}" class="btn btn-outline-danger">Cancel</a>
                                        &nbsp;
                                        <button type="submit" class="btn btn-success">
                                            Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<script>
    $( function() {
        $('#datepicker').datepicker(
            { dateFormat: 'dd/mm/yy' }
            );        
        $('#datepicker').datepicker('setDate', 'today');;        
    });
    $(".change_wallet").click(function() {
        var temp = $(this).data("value");
        var url = window.location.href.slice(0, window.location.href.lastIndexOf('/'));
        $(location).attr('href', url + '/' + temp);
    })
    $(".selectCategory").click(function() {
        var id = $(this).data("id");
        var image = $(this).data("image");
        var name = $(this).data("name");
        $("#categoryId").val(id);
        $("#imageCategory").attr("src", "{{asset('images/wallet')}}/" + image);
        $("#mediumModal").trigger("click");
        document.getElementById("nameCategory").innerHTML = name;
    })
    $("#money").keyup(function() {
        var temp = $(this).val().replace(new RegExp(',', 'g'), "");
        var number = temp.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $(this).val(number);
    })
    $("#formAddTransaction").submit(function() {
        var temp = $("#money").val().replace(new RegExp(',', 'g'), "");
        $("#money").val(temp);
    })
</script>
@stop()