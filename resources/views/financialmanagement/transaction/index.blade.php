@extends('master')
@section('content')
<div class="content" style="margin: 0 0 40% 0">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('search-transaction',['wallet_id'=>$wallet_id])}}" method="GET" id="myForm">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="cc-exp" class="control-label mb-1">Wallet</label>
                                <input id="cc-exp" hidden name="wallet_id_transaction" type="number" class="form-control cc-exp" value="{{$wallet_selected[0]->id}}">
                                <button type="button" class="btn btn-outline-success mb-1" title="{{$wallet_selected[0]->name}}" data-toggle="modal" data-target="#smallmodal" style="width: 250px">
                                    <img style="margin-left: -60px" src="{{asset('images/wallet')}}/{{$wallet_selected[0]->image_name}}" class="img-responsive" height="50">&nbsp;
                                    <b>{{ $wallet_selected[0]->name }}</b>
                                </button>
                                <div class="modal fade" id="smallmodal" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        @foreach($wallets as $w)
                                                        <div class="{{ ($wallet_id == $w->id) ? 'selected_wallet' : '' }}">
                                                            <div class="stat-widget-four change_wallet" data-value="{{$w->id}}">
                                                                <div class="stat-icon dib">
                                                                    <img src="{{asset('images/wallet')}}/{{$w->image_name}}" height="50">
                                                                </div>
                                                                <div class="stat-content">
                                                                    <div class="text-left dib">
                                                                        <div class="stat-heading">{{$w->name}}</div>
                                                                        <div class="stat-text">
                                                                            {{number_format($w->amount_of_money)}} <b>VNĐ</b>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div><!-- .row -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="categoryName" class="control-label mb-1">Category</label>
                                <input id="categoryName" name="category_name" hidden type="text" class="form-control cc-exp " value="{{(!empty($category_name) ? $category_name : '')}}">
                                <button type="button" class="btn btn-outline-success mb-1" data-toggle="modal" data-target="#mediumModal" style="width: 250px;">
                                    <img style="margin-left: -30px" src="{{$src_origin}}{{$src}}" class="img-responsive" id="imageCategory" height="50">&nbsp;
                                    <b id="nameCategory">{{(!empty($category_name) ? $category_name : 'All Categories')}}</b>
                                </button>
                                <div style="color: red">
                                    @if($errors->has('category_id'))
                                    {{$errors->first('category_id')}}
                                    @endif
                                </div>
                                <div class="modal fade" id="mediumModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="mediumModalLabel"><b>Select Category</b></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <!-- begin -->
                                                                <div class="offset-md-4">
                                                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active show" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Expense</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Income</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="tab-content" id="pills-tabContent">
                                                                    <div class="tab-pane fade active show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                                                        <?php Helpers::showAllCategoriesTransaction($wallet_id, $categories_expense) ?>
                                                                    </div>
                                                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                                        <?php Helpers::showAllCategoriesTransaction($wallet_id, $categories_income) ?>
                                                                    </div>
                                                                </div>
                                                                <!-- end -->
                                                            </div><!-- /# card -->
                                                        </div>
                                                    </div><!-- .row -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label for="datepicker_from" class="control-label mb-1">Start Date</label>
                                <input type="text" id="datepicker_from" style="height: 65px" name="start_date" class="form-control cc-name valid" placeholder="Choose start date">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label for="datepicker_to" class="control-label mb-1">End Date</label>
                                <input type="text" id="datepicker_to" style="height: 65px" name="end_date" class="form-control cc-name valid" placeholder="Choose end date">
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" id="submit" class="btn btn-success" disabled="disabled">
                            Search
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <h4 class="box-title">&nbsp;<b>Transactions</b></h4>
                    &nbsp;
                    <div style="margin: 2px 0px 0px 250px">
                        <div class="row">
                            <h4 class="box-title"><b>Income</b>: {{number_format($income)}} VNĐ</h4>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <h4 class="box-title"><b>Expense</b>: {{number_format($expense)}} VNĐ</h4>
                        </div>

                    </div>
                </div>
                <a href="{{ route('add-transaction',['wallet_id' => $wallet_id]) }}" class="btn btn-success" style="margin: -52px 0px 0px 910px "><b>Add transaction</b></a>
                <div class="table-stats order-table ov-h">
                    <table class="table ">
                        <thead>
                            <tr>
                                <th class="serial">#</th>
                                <th class="avatar">Image</th>
                                <th>Category name</th>
                                <th>Type</th>
                                <th>Money</th>
                                <th>Created day</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = 1; ?>
                            @foreach($transactionsPaginate as $te)
                            <?php $number = substr(rand(), 0, 4); ?>
                            @if($te->category_status == 0)
                            <?php $expense = $expense + $te->money ?>
                            @endif
                            @if($te->category_status == 1)
                            <?php $income += $te->money ?>
                            @endif
                            <tr>
                                <td class="serial">{{$n}}</td>
                                <td class="avatar">
                                    <div class="round-img">
                                        <a><img class="rounded-circle" src="{{asset('images/wallet')}}/{{$te->image_name}}" alt=""></a>
                                    </div>
                                </td>
                                <td> <span class="name">{{$te->category_name}}</span> </td>
                                <td>
                                    @if($te->category_status == 0)
                                    Expense
                                    @else
                                    Income
                                    @endif
                                </td>
                                <td>
                                    @if($te->category_status == 0)
                                    -
                                    @else
                                    +
                                    @endif
                                    <span class="count">{{number_format($te->money)}} VNĐ</span>
                                </td>
                                <td> <span class="product">{{ date("d/m/Y   ",strtotime($te->created_at)) }}</span> </td>
                                <td>
                                    <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal{{$number}}"><i class="fa fa-eye"></i></a>
                                    <a href="{{route('update-transaction',['wallet_id' => $wallet_id,'transaction_id' => $te->id])}}" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>
                                    <a href="{{route('delete-transaction',['wallet_id'=>$wallet_id, 'transaction_id'=>$te->id])}}" onclick="return confirm('Are you sure want to delete this transaction ?')" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                            <!-- modal expense begin -->
                            <div class="modal fade" id="modal{{$number}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="mediumModalLabel"><b>Transaction Details</b></h5>
                                            <b class="pull-right" style="margin-top: -24px">{{ date("d/m/Y",strtotime($te->created_at)) }}</b>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <img src="{{asset('images/wallet')}}/{{$te->image_name}}" height="80px">
                                                    <div class="row">
                                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                        </div>
                                                        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" style="margin-top: -65px">
                                                            <b>{{$te->category_name}}</b>
                                                            <br>
                                                            <b>
                                                                @if($te->category_status == 0)
                                                                -
                                                                @else
                                                                +
                                                                @endif
                                                                {{number_format($te->money)}} VNĐ
                                                            </b>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                    <br>
                                                    <b>NOTE: </b>
                                                    <b>{{$te->note}}</b>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal expense end -->
                            <?php $n++; ?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- /.card -->
    </div>
    <div style="margin: 0px 0px 0px 500px">
        {{$transactionsPaginate->links()}}
    </div>
</div>
<script>
    $(".change_wallet").click(function() {
        var temp = $(this).data("value");
        var url = window.location.href.slice(0, window.location.href.lastIndexOf('/'));
        $(location).attr('href', url + '/' + temp);
    })
</script>
<script>
    $(function() {
        $('#datepicker_from').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        $('#datepicker_to').datepicker({
            dateFormat: 'dd/mm/yy'
        });
        $('#datepicker_from').keypress(function(d) {
            d.preventDefault();
        });
        $('#datepicker_to').keypress(function(d) {
            d.preventDefault();
        });
    });
    $('#myForm input[type="text"]').on('input change', function() {
        if ($('#datepicker_from').datepicker('getDate') != null && $('#datepicker_to').datepicker('getDate') != null) {
            if ($('#datepicker_from').datepicker('getDate') < $('#datepicker_to').datepicker('getDate')) {
                $('#submit').prop('disabled', false);
            }
            if ($('#datepicker_from').datepicker('getDate') > $('#datepicker_to').datepicker('getDate')) {
                $('#submit').prop('disabled', true);
            }
            if ($('#datepicker_from').val() == $('#datepicker_to').val()) {
                $('#submit').prop('disabled', true);
            }
        } else {
            $('#submit').prop('disabled', true);
        }
    });
    $('#submit').prop('disabled', false);

    $(".change_wallet").click(function() {
        var temp = $(this).data("value");
        var url = window.location.href.slice(0, window.location.href.lastIndexOf('/'));
        $(location).attr('href', url + '/' + temp);
    })
    $(".selectCategory").click(function() {
        var id = $(this).data("id");
        var image = $(this).data("image");
        var name = $(this).data("name");
        $("#categoryName").val(name);
        $("#imageCategory").attr("src", "{{asset('images/wallet')}}/" + image);
        $("#mediumModal").trigger("click");
        document.getElementById("nameCategory").innerHTML = name;
    })
</script>
@stop