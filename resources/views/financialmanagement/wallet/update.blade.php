@extends('master')
@section('content')
<div class="content" style="margin: 0 0 30% 0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Update your wallet</strong>
                    </div>
                    <div class="card-body">
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="text-center">Update Wallet</h3>
                                </div>
                                <hr>
                                <form action="" method="post" id="formUpdateWallet">
                                    @csrf
                                    <div class="form-group">
                                        <input name="user_id" hidden type="text" class="form-control" value="{{Auth::id()}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Wallet name</label>
                                        <input name="name" type="text" class="form-control" value="{{$wallet[0]->name}}">
                                        <div style="color: red">
                                            @if($errors->has('name'))
                                            {{$errors->first('name')}}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="cc-name" class="control-label mb-1">Initial Balance</label>
                                        <input id="amount_of_money" name="amount_of_money" type="text" class="form-control" value="{{number_format($wallet[0]->amount_of_money)}}"> 
                                        <div style="color: red">
                                            @if($errors->has('amount_of_money'))
                                            {{$errors->first('amount_of_money')}} 
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-number" class="control-label mb-1">Wallet avatar</label>&nbsp;&nbsp;
                                        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#staticModal">
                                            <input type="text" id="image_id" hidden name="image_id" value="{{$wallet[0]->image_id}}">
                                            <img id="preview_image" src="{{asset('images/wallet')}}/{{$wallet[0]->image_name}}" height="50">
                                        </button>
                                        <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="staticModalLabel">Choose avatar</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        @foreach($images as $i)
                                                        <img data-value="{{$i->id}}" class="select_image" src="{{asset('images/wallet')}}/{{$i->name}}" height="50">
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('my-wallet') }}" class="btn btn-outline-danger">Cancel</a>
                                        &nbsp;
                                        <button type="submit" class="btn btn-success">
                                            Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".select_image").click(function() {
        var src = $(this).attr("src");
        $("#preview_image").attr('src', src);
        $("#image_id").val($(this).data("value"));
        $("#staticModal").trigger("click");
    });
    $("#amount_of_money").keyup(function() {
        var temp = $(this).val().replace(new RegExp(',', 'g'), "");
        var number = temp.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $(this).val(number);
    })
    $("#formUpdateWallet").submit(function() {
        var temp = $("#amount_of_money").val().replace(new RegExp(',', 'g'), "");
        $("#amount_of_money").val(temp);
    })
</script>
@stop()