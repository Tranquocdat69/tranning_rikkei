@extends('master')
@section('content')
<div class="content" style="margin: 0 0 40% 0">
    @if($wallets_of_user > 0 && $wallets_of_user < 3) <a href="{{route('add-wallet')}}" class="btn btn-success pull-right"><b>Add wallet</b></a>
        @else
        <a class="btn btn-success pull-right" data-toggle="modal" data-target="#errorWallet"><b style="color: white">Add wallet</b></a>
        <div class="modal fade" id="errorWallet" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            <b style="color: red">Max total wallet is 3. You can not add another wallet !!!</b>
                            <br><br>
                            <i class="fa fa-hand-o-right" style="font-size: 20px; color: #28a745"></i>
                            <b style="color: #28a745">Just remove 1 wallet before add new wallet</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="animated fadeIn">
            <div class="buttons">
                <!-- Left Block -->

                <div class="row">
                    @if (Session::has('error'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Title!</strong> {{ Session::get('error') }}
                    </div>
                    @endif
                    <div class="col-md-8 offset-md-2">
                        <div class="card">
                            <div class="card-header">
                                <strong>My wallet</strong>
                            </div>
                            <div class="card-body">
                                @foreach($wallets as $w)
                                <?php $number = substr(rand(), 0, 4) ?>
                                <a data-toggle="modal" data-target="#modal{{$number}}">
                                    <div class="stat-widget-four">
                                        <div class="stat-icon dib">
                                            <img src="{{asset('images/wallet')}}/{{$w->image_name}}" height="50">
                                        </div>
                                        <div class="stat-content">
                                            <div class="text-left dib">
                                                <div class="stat-heading">{{$w->name}}</div>
                                                <div class="stat-text">
                                                    {{number_format($w->amount_of_money)}} <b>VNĐ</b></div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </a>
                                <div class="modal fade" id="modal{{$number}}" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="scrollmodalLabel"><b>Wallet details</b></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <b>Wallet name: </b>{{$w->name}}
                                                <br><br>
                                                <b>Amount of money: </b>{{number_format($w->amount_of_money)}} <b>VNĐ</b>
                                            </div>
                                            <div class="modal-footer">
                                                    <a href="{{route('transactions',['wallet_id'=>$w->id])}}" class="btn btn-info" style="margin-right: 440px">View transactions</a>
                                                <a style="color: white" data-toggle="modal" data-target="#mediumModal{{$number}}" data-dismiss="modal" aria-label="Close" class="btn btn-danger">Delete</a>
                                                &nbsp;
                                                <a href="{{route('update-wallet',['id'=>$w->id])}}" class="btn btn-success">Update</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="mediumModal{{$number}}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" style="display: none;" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="mediumModalLabel"><b>Delete Wallet</b></h5>
                                            </div>
                                            @if($count > 1)
                                            <div class="modal-body">
                                                <b>Are you sure when you delete wallet {{$w->name}}?</b>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-outline-success" data-dismiss="modal">Cancel</button>
                                                &nbsp;
                                                <a href="{{route('delete-wallet',['id'=>$w->id])}}" class="btn btn-danger">Delete</a>
                                            </div>
                                            @endif
                                            @if($count == 1)
                                            <div class="modal-body">
                                                <b>You can not delete {{$w->name}} because this is the last wallet !!!</b>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-success" data-dismiss="modal">Got it</button>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div><!-- /# card -->
                    </div>
                </div><!-- .row -->
            </div> <!-- .buttons -->
        </div><!-- .animated -->
</div>
@stop