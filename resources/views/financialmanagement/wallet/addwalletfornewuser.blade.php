<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FiWork Managements</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/w2ui-1.5.rc1.min.css') }}">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
    @yield('css')
</head>
 
<body>
<div class="content" style="margin: 0 0 30% 0">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Create your wallet</strong>
                    </div>
                    <div class="card-body">
                        <div id="pay-invoice">
                            <div class="card-body">
                                <div class="card-title">
                                    <h3 class="text-center">Add Wallet</h3>
                                </div>
                                <hr>
                                <form action="" id="formCreateWallet" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input name="user_id" hidden type="text" class="form-control" value="{{Auth::id()}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-payment" class="control-label mb-1">Wallet name</label>
                                        <input name="name" type="text" class="form-control" value="{{old('name')}}">
                                        <div style="color: red">
                                            @if($errors->has('name'))
                                            {{$errors->first('name')}}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label for="cc-name" class="control-label mb-1">Initial Balance</label>
                                        <input name="amount_of_money" type="text" class="form-control amount_of_money" value="{{old('amount_of_money')}}">
                                        <div style="color: red">
                                            @if($errors->has('amount_of_money'))
                                            {{$errors->first('amount_of_money')}}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cc-number" class="control-label mb-1">Wallet avatar</label>&nbsp;&nbsp;
                                        <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#staticModal">
                                            <input type="text" id="image_id" hidden name="image_id" value="1">
                                            <img id="preview_image" src="{{asset('images/wallet/1.png')}}" height="50">
                                        </button>
                                        <div class="modal fade" id="staticModal" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="staticModalLabel">Choose avatar</h5>
                                                    </div>
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        @foreach($images as $i)
                                                        <img data-value="{{$i->id}}" class="select_image" src="{{asset('images/wallet')}}/{{$i->name}}" height="50">
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-success">
                                            Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(".select_image").click(function() {
        var src = $(this).attr("src");
        $("#preview_image").attr('src', src);
        $("#image_id").val($(this).data("value"));
        $("#staticModal").trigger("click");
    });
    $(".amount_of_money").keyup(function() {
        var temp = $(this).val().replace(new RegExp(',', 'g'), "");
        var number = temp.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $(this).val(number);
    });
    $("#formCreateWallet").submit(function() {
        var temp = $(".amount_of_money").val().replace(new RegExp(',', 'g'), "");
        $(".amount_of_money").val(temp);
    })
</script>
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>

    @yield('js')
</body>

</html>