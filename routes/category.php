<?php
Route::namespace('FinancialManagement')->prefix('category')->group(function(){
    Route::get('/{wallet_id}','CategoryController@index')->name('category');
    Route::get('/add-category/{wallet_id}/{status}','CategoryController@addCategory')->name('add-category');
    Route::post('/add-category/{wallet_id}/{status}','CategoryController@postAddCategory')->name('add-category');
    Route::get('/update-category/{wallet_id}/{category_id}/{status}','CategoryController@updateCategory')->name('update-category');
    Route::post('/update-category/{wallet_id}/{category_id}/{status}','CategoryController@postupdateCategory')->name('update-category');
    Route::get('/delete-category/{wallet_id}/{category_id}','CategoryController@deleteCategory')->name('delete-category');
});

?>