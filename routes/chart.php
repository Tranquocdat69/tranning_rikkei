<?php
Route::namespace('FinancialManagement')->prefix('category')->group(function () {
    Route::get('chart/{wallet_id}', 'ChartController@index')->name('chart');
    Route::get('search_chart/{wallet_id}', 'ChartController@search')->name('search-chart');
});
