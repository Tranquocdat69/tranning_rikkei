<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('name');
            $table->timestamps();
        });

        DB::table('image')->insert(
            array(
                [
                    'name' => '1.png',
                ],
                [
                    'name' => '2.png',
                ],
                [
                    'name' => '3.png',
                ],
                [
                    'name' => '4.png',
                ],
                [
                    'name' => '5.png',
                ],
                [
                    'name' => '6.png',
                ],
                [
                    'name' => '7.png',
                ],
                [
                    'name' => '8.png',
                ],
                [
                    'name' => '9.png',
                ],
                [
                    'name' => '10.png',
                ],
                [
                    'name' => '11.png',
                ],
                [
                    'name' => '12.png',
                ],
                [
                    'name' => '13.png',
                ],
                [
                    'name' => '14.png',
                ],
                [
                    'name' => '15.png',
                ],
                [
                    'name' => '16.png',
                ]
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
    }
}
