<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->bigInteger('wallet_id')->nullable()->unsigned();
            $table->foreign('wallet_id')->references('id')->on('wallet')->onDelete('cascade');
            $table->integer('parent_id');
            $table->bigInteger('image_id')->unsigned();
            $table->foreign('image_id')->references('id')->on('image')->onDelete('cascade');
            $table->smallInteger('status')->comment('0:chi,1:thu');
            $table->timestamps();
        });

        DB::table('category')->insert(
            array(
                [
                    'name' => 'Hóa đơn và Tiện ích',
                    'wallet_id' => null,
                    'parent_id' => 0,
                    'image_id' => 15,
                    'status' => 0
                ],
                [
                    'name' => 'Tiền điện',
                    'wallet_id' => null,
                    'parent_id' => 1,
                    'image_id' => 15,
                    'status' => 0
                ],
                [
                    'name' => 'Tiền nước',
                    'wallet_id' => null,
                    'parent_id' => 1,
                    'image_id' => 15,
                    'status' => 0
                ],
                [
                    'name' => 'Tiền nhà',
                    'wallet_id' => null,
                    'parent_id' => 1,
                    'image_id' => 15,
                    'status' => 0
                ],
                [
                    'name' => 'Mua sắm',
                    'wallet_id' => null,
                    'parent_id' => 0,
                    'image_id' => 3,
                    'status' => 0
                ],
                [
                    'name' => 'Thức ăn',
                    'wallet_id' => null,
                    'parent_id' => 5,
                    'image_id' => 3,
                    'status' => 0
                ],
                [
                    'name' => 'Quần áo',
                    'wallet_id' => null,
                    'parent_id' => 5,
                    'image_id' => 3,
                    'status' => 0
                ],
                [
                    'name' => 'Giày dép',
                    'wallet_id' => null,
                    'parent_id' => 5,
                    'image_id' => 3,
                    'status' => 0
                ],
                [
                    'name' => 'Tiền lương',
                    'wallet_id' => null,
                    'parent_id' => 0,
                    'image_id' => 7,
                    'status' => 1
                ],
                [
                    'name' => 'Tiền thưởng',
                    'wallet_id' => null,
                    'parent_id' => 0,
                    'image_id' => 14,
                    'status' => 1
                ],
                [
                    'name' => 'Tiền nhặt được',
                    'wallet_id' => null,
                    'parent_id' => 0,
                    'image_id' => 12,
                    'status' => 1
                ]
            )
        );
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
