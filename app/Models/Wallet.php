<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallet';

    protected $fillable = [
        'user_id', 'image_id', 'name','amount_of_money','created_at','updated_at',
    ];
 
    public function category()
    {
        return $this->hasMany('App\Models\Category','wallet_id');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\Image','image_id');
    }
}
