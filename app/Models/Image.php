<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';

    protected $fillable = [
        'name','created_at','updated_at',
    ];

    public function wallet()
    {
        return $this->hasMany('App\Models\Wallet','image_id');
    }
}
