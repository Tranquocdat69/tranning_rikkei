<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        'name', 'wallet_id', 'image_id', 'parent_id', 'status', 'created_at', 'updated_at',
    ];

    public function trasaction()
    {
        return $this->hasMany('App\Models\Trasaction', 'category_id');
    }
}
