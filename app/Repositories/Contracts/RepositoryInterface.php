<?php

namespace App\Repositories\Contracts;

interface RepositoryInterface
{
    /**
     * Get all
     * @return mixed
     */
    public function getAll();

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Create
     * @param arr $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * Update
     * @param arr $data
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id);

    /**
     * Delete
     * @param int $id
     * @return mixed
     */
    public function delete($id);

    /**
     * join table wallet and image
     * @param int $user_id
     * @param int $wallet_id
     * @return mixed
     */
    public function joinWalletAndImage($user_id, $wallet_id = null);

    /**
     * join table category and image
     * 
     * @param int $status
     * @param int $wallet_id
     * @param int $category_id
     * @return mixed
     */
    public function joinCategoryAndImage($status, $wallet_id, $category_id = null);

    /**
     * update table category
     * 
     * @param $categories
     * @param $category
     * @param $rq
     */
    public function updateCategory($categories, $category, $rq);

    /**
     * get transactions 
     * 
     * @param int $wallet_id
     * @param int $transaction_id
     */
    public function getTransactions($wallet_id, $transaction_id = null);

    /**
     * get transactions 
     * 
     * @param int $wallet_id
     * @param int $transaction_id
     */
    public function getTransactionsPaginate($wallet_id, $transaction_id = null);

    /**
     * add transactions
     * 
     * @param array $rq
     */
    public function addTransaction($wallet_id, $rq);

    /**
     * add wallet
     * 
     *  @param array $rq
     *  @param int $user_id
     */
    public function addWallet($rq, $user_id);

    /**
     * add user 
     * 
     *  @param array $rq
     */
    public function registerUser($rq);

    /**
     * update Transaction
     * @param $transaction_id
     * @param $rq
     * @param $status_category_transaction
     */
    public function updateTransaction($wallet_id, $transaction_id, $rq, $status_category_transaction);
    /**
     * delete transaction
     * 
     * @param $transaction_id
     */
    public function deleteTransaction($wallet_id, $transaction_id);

    /**
     * update wallet
     * 
     * @param int $wallet_id
     */
    public function updateWallet($wallet_id, $rq);

    /**
     * delete category
     * 
     * @param int  $wallet_id
     * @param int  $category_id
     * @param array $categories
     */
    public function deleteCategory($wallet_id, $category_id, $categories);

    /**
     * search transactions
     * 
     * @param Array $rq
     * 
     * @return mixed
     */
    public function searchTransaction($rq);

    /**
     * search transactions
     * 
     * @param Array $rq
     * 
     * @return mixed
     */
    public function searchTransactionPaginate($rq);

    /**
     * getCategoryByName
     * 
     * @param String $category_name
     * 
     * @return mixed
     */
    public function getCategoryByName($category_name);

    /**
     * @param int wallet_id
     * @param int status
     * @param Date $start_date
     * @param Date $end_date
     */
    public function getDataForChart($wallet_id, $status, $start_date, $end_date);
}
