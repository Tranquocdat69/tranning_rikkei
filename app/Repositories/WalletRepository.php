<?php

namespace App\Repositories;

use App\Repositories\EloquentRepository;

class WalletRepository extends EloquentRepository
{
	/**
	 * 
	 */
	public function getModel()
	{
		return \App\Models\Wallet::class;
	}
}
