<?php

namespace App\Repositories;

use App\Repositories\EloquentRepository;

/**
 * 
 */
class ImageRepository extends EloquentRepository
{
    public function getModel()
    {
        return \App\Models\Image::class;
    }
}

