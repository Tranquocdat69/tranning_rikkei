<?php 

namespace App\Repositories;

class TransactionRepository extends EloquentRepository 
{
    public function getModel()
    {
        return \App\Models\Transaction::class;
    }
}
