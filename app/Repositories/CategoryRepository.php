<?php
namespace App\Repositories;

use App\Repositories\EloquentRepository;

class CategoryRepository extends EloquentRepository
{
    /**
     * 
     */
    public function getModel()
    {
        return \App\Models\Category::class; 
    }
}
