<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Repositories\Contracts\RepositoryInterface;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

abstract class EloquentRepository implements RepositoryInterface
{
	protected $model;

	/**
	 * EloquentRepository constructor.
	 */
	function __construct()
	{
		$this->setModel();
	}

	abstract public function getModel();

	public function setModel()
	{
		$this->model = app()->make($this->getModel());
	}

	/**
	 * Get All
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function getAll()
	{
		return $this->model->all();
	}

	/**
	 * Get One 
	 * @param $id
	 * @return mixed
	 */
	public function find($id)
	{
		return $this->model->findOrFail($id);
	}

	/**
	 * Create
	 * @param arr $data
	 * @return mixed
	 */
	public function create(array $data)
	{
		return $this->model->create($data);
	}

	/**
	 * Update
	 * @param arr $data
	 * @param $id
	 * @return bool|mixed
	 */
	public function update(array $data, $id)
	{
		$result = $this->find($id);
		if ($result) {
			$result->update($data);
			return $result;
		}
		return false;
	}

	/**
	 * Delete
	 * 
	 * @param $id
	 * @return bool
	 */
	public function delete($id)
	{
		$result = $this->find($id);
		if ($result) {
			$result->delete($id);
			return true;
		}
		return false;
	}

	public function joinWalletAndImage($user_id, $wallet_id = null)
	{
		if ($wallet_id != null) {
			$wallets = DB::table('wallet')
				->join('image', 'image.id', '=', 'wallet.image_id')
				->select('wallet.*', 'image.name as image_name')
				->where([['wallet.id', '=', $wallet_id], ['wallet.user_id', '=', $user_id]])
				->get();
		} else {
			$wallets = DB::table('wallet')
				->join('image', 'image.id', '=', 'wallet.image_id')
				->select('wallet.*', 'image.name as image_name')
				->where('wallet.user_id', '=', $user_id)
				->get();
		}
		return $wallets;
	}

	public function getDataForChart($wallet_id, $status, $start_date, $end_date)
	{
		$data = DB::table('wallet')
			->join('transaction', 'transaction.wallet_id_transaction', '=', 'wallet.id')
			->join('category', 'transaction.category_id', '=', 'category.id')
			->select('wallet.id', DB::raw("SUM(transaction.money) as transaction_money"), 'category.name as category_name')
			->where([['wallet.id', '=', $wallet_id], ['category.status', '=', $status]])
			->whereBetween('transaction.created_at', array($start_date, $end_date))
			->groupBy('wallet.id', 'category_id')
			->distinct()
			->get();

		return $data;
	}

	public function joinCategoryAndImage($status = null, $wallet_id, $category_id = null)
	{
		if ($category_id != null) {
			$categories = DB::table('category')
				->join('image', 'image.id', '=', 'category.image_id')
				->select('category.*', 'image.name as image_name', 'image.id as image_id')
				->where([['category.id', '=', $category_id], ['category.status', '=', $status], ['category.wallet_id', '=', $wallet_id]])
				->orWhere([['category.id', '=', $category_id], ['category.status', '=', $status], ['category.wallet_id', '=', null]])
				->get();
		} else {
			$categories = DB::table('category')
				->join('image', 'image.id', '=', 'category.image_id')
				->select('category.*', 'image.name as image_name')
				->where([['category.status', '=', $status], ['category.wallet_id', '=', $wallet_id]])
				->orWhere([['category.status', '=', $status], ['category.wallet_id', '=', null]])
				->get();
		}

		return $categories;
	}
	public function updateCategory($categories, $category, $rq)
	{
		if ($category[0]->parent_id == 0 && $rq->parent_id != 0) {
			foreach ($categories as $c) {
				if ($c->parent_id == $category[0]->id) {
					$category_update = Category::find($c->id);
					$category_update->parent_id = $rq->parent_id;
					$category_update->updated_at = $rq->updated_at;
					$category_update->save();
				}
			}
		}
		$category_update =  Category::find($category[0]->id);
		$category_update->name = $rq->name;
		$category_update->parent_id = $rq->parent_id;
		$category_update->image_id = $rq->image_id;
		$category_update->updated_at = $rq->updated_at;
		$category_update->save();
	}
	public function getTransactions($wallet_id, $transaction_id = null)
	{
		if ($transaction_id == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.wallet_id_transaction', $wallet_id)
				->select('transaction.*', 'category.name as category_name', 'image.name as image_name', 'category.status as category_status')
				->get(5);
		}
		if ($transaction_id != null && $wallet_id == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.id', $transaction_id)
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'category.id as category_id', 'image.name as image_name')
				->get(5);
		}
		return $transactions;
	}
	public function getTransactionsPaginate($wallet_id, $transaction_id = null)
	{
		if ($transaction_id == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.wallet_id_transaction', $wallet_id)
				->select('transaction.*', 'category.name as category_name', 'image.name as image_name', 'category.status as category_status')
				->orderBy('transaction.created_at', 'desc')
				->paginate(5);
			}
			if ($transaction_id != null && $wallet_id == null) {
				$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.id', $transaction_id)
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'category.id as category_id', 'image.name as image_name')
				->orderBy('transaction.created_at', 'desc')
				->paginate(5);
		}
		return $transactions;
	}

	public function addTransaction($wallet_id, $rq)
	{
		$transaction = new Transaction();
		$transaction->category_id = $rq->category_id;
		$transaction->money = $rq->money;
		$transaction->wallet_id_transaction = $rq->wallet_id_transaction;
		$transaction->note = $rq->note;
		$transaction->created_at = Carbon::createFromFormat('d/m/Y', $rq->created_at)->format('Y-m-d');
		$transaction->save();
		if ($transaction) {
			$category = Category::where('id', $rq->category_id)->get();
			if ($category[0]->status == 0) {
				$wallet = Wallet::find($wallet_id);
				$wallet->amount_of_money = $wallet->amount_of_money - $rq->money;
				$wallet->save();
			}
			if ($category[0]->status == 1) {
				$wallet = Wallet::find($wallet_id);
				$wallet->amount_of_money = $wallet->amount_of_money + $rq->money;
				$wallet->save();
			}
		}
	}

	public function addWallet($rq, $user_id)
	{
		$wallet = new Wallet();
		$wallet->user_id = $rq->user_id;
		$wallet->image_id = $rq->image_id;
		$wallet->name = $rq->name;
		$wallet->amount_of_money = $rq->amount_of_money;
		$wallet->initial_balance = $rq->amount_of_money;
		$wallet->save();

		if ($wallet) {
			$user = User::find($user_id);
			$user->status = 1;
			$user->save();
		}
	}

	public function registerUser($rq)
	{
		$user = new User();
		$user->email = $rq->email;
		$user->name = $rq->name;
		$user->password = bcrypt($rq->password);
		$user->status = 0;
		$user->save();
	}

	public function updateTransaction($wallet_id, $transaction_id, $status_category_transaction, $rq)
	{
		$transaction = Transaction::find($transaction_id);
		if ($status_category_transaction == 0) {
			$category = Category::where('id', $rq->category_id)->get();
			$wallet = Wallet::find($wallet_id);
			if ($category[0]->status == 0) {
				$wallet->amount_of_money = $wallet->amount_of_money + $transaction->money;
				$wallet->amount_of_money = $wallet->amount_of_money - $rq->money;
				$wallet->save();
			}
			if ($category[0]->status == 1) {
				$wallet->amount_of_money = $wallet->amount_of_money + $transaction->money;
				$wallet->amount_of_money = $wallet->amount_of_money + $rq->money;
				$wallet->save();
			}
		}
		if ($status_category_transaction == 1) {
			$wallet = Wallet::find($wallet_id);
			$category = Category::where('id', $rq->category_id)->get();
			if ($category[0]->status == 0) {
				$wallet->amount_of_money = $wallet->amount_of_money - $transaction->money;
				$wallet->amount_of_money = $wallet->amount_of_money - $rq->money;
				$wallet->save();
			}
			if ($category[0]->status == 1) {
				$wallet->amount_of_money = $wallet->amount_of_money - $transaction->money;
				$wallet->amount_of_money = $wallet->amount_of_money + $rq->money;
				$wallet->save();
			}
		}
		$transaction->category_id = $rq->category_id;
		$transaction->money = $rq->money;
		$transaction->created_at = Carbon::createFromFormat('d/m/Y', $rq->created_at)->format('Y-m-d');
		$transaction->note = $rq->note;

		$transaction->save();
	}

	public function deleteTransaction($wallet_id, $transaction_id)
	{
		$transaction = Transaction::find($transaction_id);
		$category = Category::where('id', $transaction->category_id)->get();
		$wallet = Wallet::find($wallet_id);
		if ($category[0]->status == 0) {
			$wallet->amount_of_money = $wallet->amount_of_money + $transaction->money;
			$wallet->save();
		}
		if ($category[0]->status == 1) {
			$wallet->amount_of_money = $wallet->amount_of_money - $transaction->money;
			$wallet->save();
		}
		$transaction->delete();
	}

	public function updateWallet($wallet_id, $rq)
	{
		$income = DB::table('transaction')
			->join('category', 'category.id', '=', 'transaction.category_id')
			->where([['transaction.wallet_id_transaction', '=', $wallet_id], ['category.status', '=', 1]])
			->sum('transaction.money');
		$outcome = DB::table('transaction')
			->join('category', 'category.id', '=', 'transaction.category_id')
			->where([['transaction.wallet_id_transaction', '=', $wallet_id], ['category.status', '=', 0]])
			->sum('transaction.money');
		$wallet = Wallet::find($wallet_id);
		$wallet->name = $rq->name;
		$wallet->initial_balance = $rq->amount_of_money;
		$wallet->amount_of_money = $rq->amount_of_money;
		$wallet->amount_of_money = $wallet->amount_of_money + $income - $outcome;
		$wallet->image_id = $rq->image_id;

		$wallet->save();
	}

	public function deleteCategory($wallet_id, $category_id, $categories)
	{
		$category = Category::find($category_id);
		$wallet = Wallet::find($wallet_id);
		$transaction = DB::table('transaction')
			->join('category', 'category.id', '=', 'transaction.category_id')
			->select('transaction.*', 'category.status as category_status')
			->where([['transaction.category_id', '=', $category_id], ['transaction.wallet_id_transaction', '=', $wallet_id]])
			->get();
		if ($category->parent_id == 0) {
			$transaction_count = count($transaction);
			if ($transaction_count > 0) {
				if ($transaction[0]->category_status == 0) {
					$wallet->amount_of_money = 	$wallet->amount_of_money + $transaction[0]->money;
					$wallet->save();
				}
				if ($transaction[0]->category_status == 1) {
					$wallet->amount_of_money = 	$wallet->amount_of_money - $transaction[0]->money;
					$wallet->save();
				}
			}
			foreach ($categories as $c) {
				if ($c->parent_id == $category_id) {
					$transactions_child = DB::table('transaction')
						->join('category', 'category.id', '=', 'transaction.category_id')
						->select('transaction.*', 'category.status as category_status', 'category.name as category_name')
						->where('transaction.category_id', '=', $c->id)
						->get();
					$transactions_child_count = count($transactions_child);
					if ($transactions_child_count > 0) {
						if ($transactions_child[0]->category_status == 0) {
							$wallet->amount_of_money = $wallet->amount_of_money + $transactions_child[0]->money;
							$wallet->save();
						}
						if ($transactions_child[0]->category_status == 1) {
							$wallet->amount_of_money = $wallet->amount_of_money - $transactions_child[0]->money;
							$wallet->save();
						}
					}
					Category::where('id', $c->id)->delete();
				}
			}
		} else {
			if ($category->status == 0) {
				$wallet->amount_of_money = $wallet->amount_of_money + $transaction[0]->money;
				$wallet->save();
			}
			if ($category->status == 1) {
				$wallet->amount_of_money = $wallet->amount_of_money - $transaction[0]->money;
				$wallet->save();
			}
		}
		$category->delete();
	}

	public function searchTransaction($rq)
	{
		if ($rq->start_date != null && $rq->end_date != null) {
			$start_date = Carbon::createFromFormat('d/m/Y', $rq->start_date)->format('Y-m-d');
			$end_date = Carbon::createFromFormat('d/m/Y', $rq->end_date)->format('Y-m-d');
		}

		if ($rq->category_name != null && $rq->start_date != null && $rq->end_date != null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where([['category.name', 'like', $rq->category_name], ['transaction.wallet_id_transaction', $rq->wallet_id_transaction]])
				->whereBetween('transaction.created_at', array($start_date, $end_date))
				->select('transaction.id', 'transaction.money', 'transaction.note', 'transaction.created_at', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name', DB::raw("SUM(transaction.money) as transaction_money"))
				->groupBy('transaction.id')
				->get();
		}
		if ($rq->category_name != null && $rq->start_date == null && $rq->end_date == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where([['category.name', 'like', $rq->category_name], ['transaction.wallet_id_transaction', $rq->wallet_id_transaction]])
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name')
				->get();
		}
		if ($rq->category_name == null && $rq->start_date != null && $rq->end_date != null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.wallet_id_transaction', $rq->wallet_id_transaction)
				->whereBetween('transaction.created_at', array($start_date, $end_date))
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name')
				->get();
		}
		if ($rq->category_name == null && $rq->start_date == null && $rq->end_date == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.wallet_id_transaction', $rq->wallet_id_transaction)
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name')
				->get();
		}

		return $transactions;
	}
	public function searchTransactionPaginate($rq)
	{
		if ($rq->start_date != null && $rq->end_date != null) {
			$start_date = Carbon::createFromFormat('d/m/Y', $rq->start_date)->format('Y-m-d');
			$end_date = Carbon::createFromFormat('d/m/Y', $rq->end_date)->format('Y-m-d');
		}

		if ($rq->category_name != null && $rq->start_date != null && $rq->end_date != null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where([['category.name', 'like', $rq->category_name], ['transaction.wallet_id_transaction', $rq->wallet_id_transaction]])
				->whereBetween('transaction.created_at', array($start_date, $end_date))
				->select('transaction.id', 'transaction.money', 'transaction.note', 'transaction.created_at', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name', DB::raw("SUM(transaction.money) as transaction_money"))
				->groupBy('transaction.id')
				->paginate(1);
		}
		if ($rq->category_name != null && $rq->start_date == null && $rq->end_date == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where([['category.name', 'like', $rq->category_name], ['transaction.wallet_id_transaction', $rq->wallet_id_transaction]])
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name')
				->paginate(1);
		}
		if ($rq->category_name == null && $rq->start_date != null && $rq->end_date != null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.wallet_id_transaction', $rq->wallet_id_transaction)
				->whereBetween('transaction.created_at', array($start_date, $end_date))
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name')
				->paginate(1);
		}
		if ($rq->category_name == null && $rq->start_date == null && $rq->end_date == null) {
			$transactions = DB::table('transaction')
				->join('category', 'category.id', '=', 'transaction.category_id')
				->join('image', 'category.image_id', '=', 'image.id')
				->where('transaction.wallet_id_transaction', $rq->wallet_id_transaction)
				->select('transaction.*', 'category.name as category_name', 'category.status as category_status', 'image.name as image_name')
				->paginate(1);
		}

		return $transactions;
	}



	public function getCategoryByName($category_name)
	{
		$category = DB::table('category')
			->join('image', 'category.image_id', '=', 'image.id')
			->where('category.name', 'like', $category_name)
			->select('category.*', 'image.name as image_name')
			->get();

		return $category;
	}
}
