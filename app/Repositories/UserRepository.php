<?php 
namespace App\Repositories;

use App\Repositories\EloquentRepository;

/**
 * 
 */
class UserRepository extends EloquentRepository
{
	public function getModel()
	{
		return \App\User::class;
	}
}