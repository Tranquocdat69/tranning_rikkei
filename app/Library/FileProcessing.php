<?php 
namespace App\Library;

use File;
use Illuminate\Http\Request;
/**
 * 
 */
class FileProcessing
{

    public function __construct()
    {

    }

	public function storeFile($request)
	{
		if ($request->hasFile('avatar')) {
            $fileName = time().'-'.$request->file('avatar')->getClientOriginalName();
            $imgPath = public_path('/img/avatar');
            $request->file('avatar')->move($imgPath, $fileName);
            return $fileName;
        }

        return 'anh_demo.jpg';
	}
}