<?php

namespace App\Helpers;

use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class Helpers
{
    /**
     * show categories and their parent_id 
     *
     * @param array $categories
     * @param int $parent_id
     * @param character $symbol
     * 
     */
    public static function showAllCategories($wallet_id, $categories, $parent_id = 0, $symbol = '')
    {
        foreach ($categories as $key => $c) {
            if ($c->parent_id == $parent_id) {
                $url = asset('images/wallet') . '/' . $c->image_name;
                $url_delete = route('delete-category', ['wallet_id' => $wallet_id, 'category_id' => $c->id]);
                $url_update = route('update-category', ['wallet_id' => $wallet_id, 'category_id' => $c->id, 'status' => $c->status]);
                $number = substr(rand(), 0, 4);
                echo "<a data-toggle='modal' data-target='#modal$number'>";
                echo "<div class='stat-widget-four'>";
                echo " <div class='stat-icon dib'>";
                echo $symbol . "<img src='$url' height='40'>";
                echo "</div>";
                echo "<div class='stat-content'>";
                echo "<div class='text-left dib'>";
                echo "<div class='stat-text'>";
                echo "<div class='stat-heading' style='margin: 9px 0px 0px 100px'>$c->name</div>";
                echo "<div class='row'>";
                if ($c->wallet_id == null) {
                    echo "<i style='margin: -25px 0px 0px 450px;font-size: 25px' class='fa fa-eye-slash'></i>";
                } else {
                    echo "<i style='margin: -25px 0px 0px 450px;font-size: 25px' class='fa fa-eye'></i>";
                }
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "<hr>";
                echo "</div>";

                echo "</a>";
                if ($c->wallet_id != null) {
                    echo "<div class='modal fade catDe' id='modal$number' id='catDe' tabindex='-1' role='dialog' aria-labelledby='scrollmodalLabel' style='display: none;' aria-hidden='true' data-dismiss='modal'>";
                    echo "<div class='modal-dialog modal-lg' role='document'>";
                    echo "<div class='modal-content'>";
                    echo "<div class='modal-header'>";
                    echo "<h5 class='modal-title' id='scrollmodalLabel'><b>Category details</b></h5>";
                    echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
                    echo "<span aria-hidden='true'>×</span>";
                    echo "</button>";
                    echo "</div>";
                    echo "<div class='modal-body'>";
                    echo "<b>Category name: </b>$c->name";
                    echo "</div>";
                    echo "<div class='modal-footer'>";
                    echo "<a style='color: white' class='btn btn-danger del' data-dismiss='modal' aria-label='Close' data-toggle='modal' data-target='#delete$number'>Delete</a>";
                    echo "&nbsp;";
                    echo "<a href='$url_update' class='btn btn-success'>Update</a>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";

                    echo "<div class='modal fade' id='delete$number' tabindex='-1' role='dialog' aria-labelledby='scrollmodalLabel' style='display: none;' aria-hidden='true'>";
                    echo "<div class='modal-dialog modal-lg' role='document'>";
                    echo "<div class='modal-content'>";
                    echo "<div class='modal-header'>";
                    echo "<h5 class='modal-title' id='scrollmodalLabel'><b>Delete category</b></h5>";
                    echo "</div>";
                    echo "<div class='modal-body'>";
                    if ($c->parent_id == 0) {
                        echo "You will lose all Transactions in <b>" . $c->name . "</b> category, along with those child categories and their transactions as shown:";
                        echo "<br>";
                        $count =  DB::table('transaction')
                            ->where('category_id', '=', $c->id)
                            ->count('id');
                        if ($count > 0) {
                            echo "<b>There are " . $count . " transactions belong to this category</b>";
                        } else {
                            echo "<b>There are no transactions in this category</b>";
                        }
                        foreach ($categories as $ca) {
                            if ($ca->parent_id == $c->id) {
                                echo "<div style = 'margin-left: 60px'>";
                                $count =  DB::table('transaction')
                                    ->where('category_id', '=', $ca->id)
                                    ->count('id');
                                $url_child = asset('images/wallet') . '/' . $ca->image_name;
                                echo "<br>";
                                echo "<img src='$url_child' height='40'>";
                                echo "&nbsp";
                                echo "<b>" . $ca->name . "</b>";
                                echo "<br>";
                                if ($count > 0) {
                                    echo $count . " transactions";
                                } else {
                                    echo "0 transactions";
                                }
                                echo "</div>";
                            }
                        }
                    } else {
                        echo "Are you sure want to delete category <b> $c->name ?</b>";
                        $count =  DB::table('transaction')
                            ->where('category_id', '=', $c->id)
                            ->count('id');
                        echo "<br>";
                        if ($count > 0) {
                            echo "<b>There are " . $count . " transactions belong to this category</b>";
                        } else {
                            echo "<b>There are no transactions in this category</b>";
                        }
                    }
                    echo "</div>";
                    echo "<div class='modal-footer'>";
                    echo "<a href='$url_delete' class='btn btn-outline-success' data-dismiss='modal' >Cancel</a>";
                    echo "<a href='$url_delete' class='btn btn-danger' >Delete</a>";
                    echo "&nbsp;";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                    echo "</div>";
                }
                unset($categories[$key]);

                Helpers::showAllCategories($wallet_id, $categories, $c->id, $symbol . '----');
            }
        }
    }
    public static function showAllCategoriesTransaction($wallet_id, $categories, $parent_id = 0, $symbol = '')
    {
        foreach ($categories as $key => $c) {
            if ($c->parent_id == $parent_id) {
                $url = asset('images/wallet') . '/' . $c->image_name;
                echo "<div class='stat-widget-four selectCategory' data-id = '$c->id' data-image = '$c->image_name' data-name = '$c->name' >";
                echo " <div class='stat-icon dib'>";
                echo $symbol . "<img src='$url' height='40'>";
                echo "</div>";
                echo "<div class='stat-content'>";
                echo "<div class='text-left dib'>";
                echo "<div class='stat-text'>";
                echo "<div class='stat-heading'>$c->name</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "<hr>";
                echo "</div>";
                unset($categories[$key]);

                Helpers::showAllCategoriesTransaction($wallet_id, $categories, $c->id, $symbol . '----');
            }
        }
    }

    public static function getParentCategory($categories)
    {
        foreach ($categories as $c) {
            if ($c->parent_id == 0) {
                echo "<option value='" . $c->id . "'>" . $c->name . "</option>";
            }
        }
    }
    public static function showCategoryUpdate($categories, $category)
    {
        foreach ($categories as $key => $c) {
            if ($c->parent_id == 0) {
                if ($c->id != $category[0]->id) {
                    if ($category[0]->parent_id == $c->id) {
                        echo "<option selected value='" . $c->id . "'>" . $c->name . "</option>";
                        unset($categories[$key]);
                    } else {
                        echo "<option value='" . $c->id . "'>" . $c->name . "</option>";
                    }
                }
            }
        }
    }
}
