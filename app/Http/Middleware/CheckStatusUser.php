<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckStatusUser
{
    /**
     * Handle an incoming request.
     * 
     * @param  \Illuminate\Http\Request  $request 
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->status == 0) {
            return redirect('add-wallet-for-new-user');
        }
        if ($user->status == 2) {
            return redirect('change_pass_after_reset');
        } else {
            return $next($request);
        }
    }
}
