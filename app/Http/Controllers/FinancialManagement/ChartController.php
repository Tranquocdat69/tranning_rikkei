<?php

namespace App\Http\Controllers\FinancialManagement;

use App\Charts\DashboardChart;
use App\Http\Controllers\Controller;
use App\Repositories\WalletRepository;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ChartController extends Controller
{
    protected $walletRepository;

    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }
    public function index($wallet_id)
    {

        $date = date('d-m-Y', time());
        $start_date_date = date("01-m-Y", strtotime($date));
        $end_date_date = date("t-m-Y", strtotime($date));

        $start_date = date("Y-m-d", strtotime($start_date_date));
        $end_date = date("Y-m-d", strtotime($end_date_date));

        $wallet = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $expense = $this->walletRepository->getDataForChart($wallet_id, 0, $start_date, $end_date);
        $income = $this->walletRepository->getDataForChart($wallet_id, 1, $start_date, $end_date);

        $chart_expense = new DashboardChart;
        $chart_income = new DashboardChart;
        $categoryName_expense = $expense
            ->map(function ($item) {
                return $item->category_name;
            });
        $data_expense = $expense
            ->map(function ($item) {
                return $item->transaction_money;
            });

        $categoryName_income = $income
            ->map(function ($item) {
                return $item->category_name;
            });
        $data_income = $income
            ->map(function ($item) {
                return $item->transaction_money;
            });
        $array_expense = [];
        for ($i = 0; $i < count($data_expense); $i++) {
            $col[$i] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            array_push($array_expense, $col[$i]);
        }
        $array_income = [];
        for ($i = 0; $i < count($data_income); $i++) {
            $col[$i] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            array_push($array_income, $col[$i]);
        }
        $chart_expense->labels($categoryName_expense->values());
        $color_expense = $chart_expense->dataset('My dataset', 'pie', $data_expense->values());
        $chart_expense->title('Expense');
        $chart_expense->displayAxes(false);
        $color_expense->backgroundColor(collect($array_expense));


        $chart_income->labels($categoryName_income->values());
        $chart_income->title('Income');
        $chart_income->displayAxes(false);
        $color_income = $chart_income->dataset('My dataset', 'pie', $data_income->values());
        $color_income->backgroundColor(collect($array_income));


        return view("financialmanagement.chart.index", compact(['chart_expense', 'chart_income', 'wallet', 'start_date', 'end_date', 'wallet_id']));
    }

    public function search($wallet_id)
    {
        $start_date_get = (!empty($_GET['start_date'])) ? $_GET['start_date'] : '';
        $end_date_get = (!empty($_GET['end_date'])) ? $_GET['end_date'] : '';

        $start_date = Carbon::createFromFormat('d/m/Y', $start_date_get)->format('Y-m-d');
        $end_date = Carbon::createFromFormat('d/m/Y', $end_date_get)->format('Y-m-d');

        $wallet = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $expense = $this->walletRepository->getDataForChart($wallet_id, 0, $start_date, $end_date);
        $income = $this->walletRepository->getDataForChart($wallet_id, 1, $start_date, $end_date);

        $chart_expense = new DashboardChart;
        $chart_income = new DashboardChart;
        $categoryName_expense = $expense
            ->map(function ($item) {
                return $item->category_name;
            });
        $data_expense = $expense
            ->map(function ($item) {
                return $item->transaction_money;
            });

        $categoryName_income = $income
            ->map(function ($item) {
                return $item->category_name;
            });
        $data_income = $income
            ->map(function ($item) {
                return $item->transaction_money;
            });
        $array_expense = [];
        for ($i = 0; $i < count($data_expense); $i++) {
            $col[$i] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            array_push($array_expense, $col[$i]);
        }
        $array_income = [];
        for ($i = 0; $i < count($data_income); $i++) {
            $col[$i] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            array_push($array_income, $col[$i]);
        }
        $chart_expense->labels($categoryName_expense->values());
        $color_expense = $chart_expense->dataset('My dataset', 'pie', $data_expense->values());
        $chart_expense->title('Expense');
        $chart_expense->displayAxes(false);
        $color_expense->backgroundColor(collect($array_expense));


        $chart_income->labels($categoryName_income->values());
        $chart_income->title('Income');
        $chart_income->displayAxes(false);
        $color_income = $chart_income->dataset('My dataset', 'pie', $data_income->values());
        $color_income->backgroundColor(collect($array_income));




        return view("financialmanagement.chart.index", compact(['chart_expense', 'chart_income', 'wallet', 'start_date', 'end_date', 'wallet_id']));
    }
}
