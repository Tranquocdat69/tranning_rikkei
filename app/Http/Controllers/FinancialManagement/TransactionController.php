<?php

namespace App\Http\Controllers\FinancialManagement;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestAddTransaction;
use App\Http\Requests\RequestSearchTransaction;
use App\Http\Requests\RequestUpdateTransaction;
use App\Repositories\CategoryRepository;
use App\Repositories\ImageRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\WalletRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    protected $walletRepository;
    protected $transactionRepository;
    protected $imageRepository;
    protected $categoryRepository;

    public function __construct(WalletRepository $walletRepository, TransactionRepository $transactionRepository, ImageRepository $imageRepository, CategoryRepository $categoryRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->transactionRepository = $transactionRepository;
        $this->imageRepository = $imageRepository;
        $this->categoryRepository = $categoryRepository;
    }
    public function index($wallet_id, Request $rq)
    {
        $category_name = (!empty($_GET['category_name'])) ? $_GET['category_name'] : '';
        $start_date = (!empty($_GET['start_date'])) ? $_GET['start_date'] : '';
        $end_date = (!empty($_GET['end_date'])) ? $_GET['end_date'] : '';
        if (!empty($category_name)) {
            $category = $this->categoryRepository->getCategoryByName($category_name);
            $src_origin = asset('images/wallet') . "/";
            $src = $category[0]->image_name;
            $cat_id = $category[0]->id;
        } else { 
            $src_origin = asset('images') . "/";
            $src = "favicon.png";
            $cat_id = "";
        }

        $wallets = $this->walletRepository->joinWalletAndImage(Auth::id());
        $wallet_selected = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $transactions = $this->transactionRepository->getTransactions($wallet_id);
        $transactionsPaginate = $this->transactionRepository->getTransactionsPaginate($wallet_id);
        $categories_expense = $this->categoryRepository->joinCategoryAndImage(0, $wallet_id);
        $categories_income = $this->categoryRepository->joinCategoryAndImage(1, $wallet_id);
        $income = 0;
        $expense = 0;
        foreach ($transactions as $tr) {
            if ($tr->category_status == 0) {
                $expense += $tr->money;
            }
            if ($tr->category_status == 1) {
                $income += $tr->money;
            }
        }
        return view("financialmanagement.transaction.index", compact(['transactionsPaginate', 'transactions','wallets', 'wallet_selected', 'wallet_id', 'categories_expense', 'categories_income', 'src_origin', 'src', 'transaction_search', 'category_name', 'income', 'expense']));
    }

    public function addTransaction($wallet_id)
    {
        $wallets = $this->walletRepository->joinWalletAndImage(Auth::id());
        $wallet_selected = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $categories_expense = $this->categoryRepository->joinCategoryAndImage(0, $wallet_id);
        $categories_income = $this->categoryRepository->joinCategoryAndImage(1, $wallet_id);
        $date = date('Y-m-d');
        return view("financialmanagement.transaction.add", compact(['date', 'wallet_id', 'wallets', 'wallet_selected', 'categories_expense', 'categories_income']));
    }

    public function postAddTransaction($wallet_id, RequestAddTransaction $rq)
    {
        $rq->offsetUnset('_token');
        $this->transactionRepository->addTransaction($wallet_id, $rq);

        return redirect()->route('transactions', ['wallet_id' => $wallet_id]);
    }
 
    public function updateTransaction($wallet_id, $transaction_id)
    {
        $transaction = $this->transactionRepository->getTransactions(null, $transaction_id);
        $wallet_selected = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $categories_expense = $this->categoryRepository->joinCategoryAndImage(0, $wallet_id);
        $categories_income = $this->categoryRepository->joinCategoryAndImage(1, $wallet_id);
        return view("financialmanagement.transaction.update", compact(['transaction', 'wallet_selected', 'wallet_id', 'categories_expense', 'categories_income']));
    }

    public function postUpdateTransaction($wallet_id, $transaction_id, RequestUpdateTransaction $rq)
    {
        $transaction = $this->transactionRepository->getTransactions(null, $transaction_id);
        $this->transactionRepository->updateTransaction($wallet_id, $transaction_id, $transaction[0]->category_status, $rq);
        return redirect()->route('transactions', ['wallet_id' => $wallet_id]);
    }

    public function deleteTransaction($wallet_id, $transaction_id)
    {
        $this->transactionRepository->deleteTransaction($wallet_id, $transaction_id);
        return redirect()->route('transactions', ['wallet_id' => $wallet_id]);
    }

    public function searchTransaction($wallet_id, Request $rq)
    {
        $category_name = (!empty($_GET['category_name'])) ? $_GET['category_name'] : '';
        $start_date = (!empty($_GET['start_date'])) ? $_GET['start_date'] : '';
        $end_date = (!empty($_GET['end_date'])) ? $_GET['end_date'] : '';
        if (!empty($category_name)) {
            $category = $this->categoryRepository->getCategoryByName($category_name);
            $src_origin = asset('images/wallet') . "/";
            $src = $category[0]->image_name;
            $cat_id = $category[0]->id;
        } else {
            $src_origin = asset('images') . "/";
            $src = "favicon.png";
            $cat_id = "";
        }
        $wallets = $this->walletRepository->joinWalletAndImage(Auth::id());
        $wallet_selected = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $categories_expense = $this->categoryRepository->joinCategoryAndImage(0, $wallet_id);
        $categories_income = $this->categoryRepository->joinCategoryAndImage(1, $wallet_id);
        $transactions = $this->transactionRepository->searchTransaction($rq);
        $transactionsPaginate = $this->transactionRepository->searchTransactionPaginate($rq); 
        $income = 0;
        $expense = 0;
        foreach ($transactions as $tr) {
            if ($tr->category_status == 0) {
                $expense += $tr->money;
            }
            if ($tr->category_status == 1) {
                $income += $tr->money;
            }
        }
        return view("financialmanagement.transaction.search", compact(['wallet_selected', 'wallets', 'wallet_id', 'categories_expense', 'categories_income', 'transactionsPaginate', 'transactions','category', 'start_date', 'end_date', 'src_origin', 'src', 'cat_id', 'symbol', 'category_name', 'income', 'expense']));
    }
}
