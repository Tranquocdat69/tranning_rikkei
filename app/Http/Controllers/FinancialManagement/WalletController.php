<?php

namespace App\Http\Controllers\FinancialManagement;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestAddWallet;
use App\Http\Requests\RequestUpdateWallet;
use App\Models\Wallet;
use App\Repositories\ImageRepository;
use App\Repositories\WalletRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{
    protected $walletRepository;
    protected $imageRepository;

    public function __construct(WalletRepository $walletRepository, ImageRepository $imageRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->imageRepository = $imageRepository;
    }
    /**
     * Show all wallets
     * 
     * @return view index of wallet
     */
    public function index()
    {
        $wallets = $this->walletRepository->joinWalletAndImage(Auth::id());
        $count = count($wallets);
        $wallets_of_user = Wallet::where('user_id', Auth::id())->count();
        return view("financialmanagement.wallet.index", compact(['wallets','wallets_of_user','count']));
    }

    /**
     * Show form to user type in
     * 
     * @return view add of wallet
     */
    public function addWallet()
    {
        $images = $this->imageRepository->getAll();
        return view("financialmanagement.wallet.add", compact(['images']));
    }
    /**
     * get data from form to insert into table wallet
     *  
     * @param Request $rq
     * @return view index of wallet
     */
    public function postAddWallet(RequestAddWallet $rq)
    {
        $rq->offsetUnset('_token');
        $this->walletRepository->addWallet($rq, Auth::id());

        return redirect()->route('my-wallet');
    }
    /**
     * Show form to user type in
     * 
     * @return view add of wallet
     */
    public function addWalletForNewUser()
    {
        $images = $this->imageRepository->getAll();

        return view("financialmanagement.wallet.addwalletfornewuser", compact(['images']));
    }
    /**
     * get data from form to insert into table wallet
     *  
     * @param Request $rq 
     * @return view index of wallet
     */
    public function postAddWalletForNewUser(RequestAddWallet $rq)
    {
        $rq->offsetUnset('_token');
        $this->walletRepository->addWallet($rq, Auth::id());

        return redirect()->route('home');
    }

    /**
     * show information of wallet that user want to update
     * 
     * @param $id
     * @return view update of wallet
     */
    public function updateWallet($id)
    {
        $wallet = $this->walletRepository->joinWalletAndImage(Auth::id(), $id);
        $images = $this->imageRepository->getAll();

        return view("financialmanagement.wallet.update", compact(['wallet', 'images']));
    }

    /**
     * update wallet
     * 
     * @param Request $rq
     * @return view index of wallet
     */
    public function postUpdateWallet(RequestUpdateWallet $rq, $id)
    {
        $rq->offsetUnset('_token');
        $this->walletRepository->updateWallet($id, $rq);

        return redirect()->route('my-wallet');
    }

    /**
     * delete wallet
     * 
     * @param $id
     * @return previous page
     */
    public function deleteWallet($id)
    {
        $this->walletRepository->delete($id);

        return redirect()->back();
    }
}
