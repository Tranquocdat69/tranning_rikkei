<?php

namespace App\Http\Controllers\FinancialManagement;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestAddCategory;
use App\Http\Requests\RequestUpdateCategory;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\ImageRepository;
use App\Repositories\WalletRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    protected $categoryRepository;
    protected $imageRepository;
    protected $walletRepository;

    public function __construct(CategoryRepository $categoryRepository, ImageRepository $imageRepository, WalletRepository $walletRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->imageRepository = $imageRepository;
        $this->walletRepository = $walletRepository;
    }
    /**
     * show all category  
     * 
     * @return view index of category 
     */
    public function index($wallet_id)
    {
        $wallets = $this->walletRepository->joinWalletAndImage(Auth::id());
        $wallet_selected = $this->walletRepository->joinWalletAndImage(Auth::id(), $wallet_id);
        $categories_expense = $this->categoryRepository->joinCategoryAndImage(0, $wallet_id);
        $categories_income = $this->categoryRepository->joinCategoryAndImage(1, $wallet_id);
        $status = 1;
        return view("financialmanagement.category.index", compact(['categories_expense', 'categories_income', 'wallets', 'wallet_selected', 'status', 'wallet_id']));
    }
    /**
     * Show form to user type in
     * 
     * @return view add of category
     */
    public function addCategory($wallet_id, $status)
    {
        $categories = $this->categoryRepository->joinCategoryAndImage($status, $wallet_id);
        $images = $this->imageRepository->getAll();
        $checked_expense = ($status == 0) ? 'checked' : '';
        $checked_income = ($status == 1) ? 'checked' : '';
        $date = date('Y-m-d'); 
        return view("financialmanagement.category.add", compact(['images', 'wallet_id', 'categories', 'checked_expense', 'checked_income', 'date']));
    }
    /**
     * get data from form and insert into table category
     * 
     * @param Request $rq
     */
    public function postAddCategory($wallet_id, RequestAddCategory $rq)
    {
        $rq->offsetUnset('_token');
        $this->categoryRepository->create($rq->all());

        return redirect()->route('category', ['wallet_id' => $wallet_id]);
    }
    public function updateCategory($wallet_id, $category_id, $status)
    {
        $category = $this->categoryRepository->joinCategoryAndImage($status, $wallet_id, $category_id);
        $categories = $this->categoryRepository->joinCategoryAndImage($status, $wallet_id);
        $images = $this->imageRepository->getAll();
        $checked_expense = ($status == 0) ? 'checked' : '';
        $checked_income = ($status == 1) ? 'checked' : '';
        $date = date('Y-m-d');
        return view("financialmanagement.category.update", compact(['checked_expense', 'checked_income', 'categories', 'images', 'date', 'wallet_id', 'category']));
    }
    public function postUpdateCategory($wallet_id, $category_id, $status, RequestUpdateCategory $rq)
    {

        $rq->offsetUnset('_token');
        $category = $this->categoryRepository->joinCategoryAndImage($status, $wallet_id, $category_id);
        $categories = $this->categoryRepository->joinCategoryAndImage($status, $wallet_id);
        $this->categoryRepository->updateCategory($categories, $category, $rq);

        return redirect()->route('category', ['wallet_id' => $wallet_id]);
    }
    public function deleteCategory($wallet_id, $category_id)
    {
        $categories = Category::all();
        $this->categoryRepository->deleteCategory($wallet_id, $category_id, $categories);
        return redirect()->back();
    }
}
