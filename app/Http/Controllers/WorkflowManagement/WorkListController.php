<?php

namespace App\Http\Controllers\WorkflowManagement;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\RequestCreateWorkList;
use App\Http\Requests\RequestEditWorkList;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Models\Group;
use App\Models\Detail;
use Auth;
use File;

class WorkListController extends Controller
{
    //Hàm chuyển sang view work list
    public function workList($groupId)
    {
        $details = DB::table('details')->where(['group_id' => $groupId])->get();
        $listDoing = DB::table('details')->where([
            ['group_id', '=', $groupId],
            ['status', '=', 1],
        ])->get();
        $listDone = DB::table('details')->where([
            ['group_id', '=', $groupId],
            ['status', '=', 2],
        ])->get();
        return view('workflowmanagement.worklist.worklist', compact(['details','groupId','listDoing','listDone']));
    }

    //Hàm chuyển sang view create things to do
    /**
     * 
     */
    public function createWorkList($groupId)
    {
        return view('workflowmanagement.worklist.createworklist', compact(['groupId']));
    }

    //Hàm post form tạo list things to do
    public function postCreateWorkList(RequestCreateWorkList $request, $groupId)
    {
        $detail = new Detail;
        $today = Carbon::now('Asia/Ho_Chi_Minh')->toDateTimeString();
        $start_date = $request->start_date ? Carbon::create($request->start_date)->toDateTimeString() : null;
        $finish_date = $request->finish_date ? Carbon::create($request->finish_date)->toDateTimeString() : null;
        $time_remind = $request->time_remind ? Carbon::create($request->time_remind)->toDateTimeString() : null;
        if ($start_date > $finish_date) {
            $request->session()->flash('error','Start date have to smaller than the finish date !');
            $request->flash();
            return redirect()->back()->withInput();
        }
        if ($time_remind) {

            if ($time_remind > $finish_date || $time_remind < $start_date) {
                $request->session()->flash('error','Your time remind wrong !');
                $request->flash();
                return redirect()->back()->withInput();
            }else{
                $detail->status_time_remind = 1;
            }
        }
        if (!$time_remind && $request->content_remind) {
            $request->session()->flash('error','You have to set up your time remind before enter content remind !');
            $request->flash();
            return redirect()->back()->withInput();
        }
        if ($finish_date && ($finish_date < $today)) {
            $detail->status = 2;
            $detail->status_time_remind = 0;
        }else{
            $detail->status = 1;
        }
        $detail->group_id = $groupId;
        $detail->name = $request->name;
        $detail->description = $request->description;
        $detail->start_date = $start_date;
        $detail->finish_date = $finish_date;
        $detail->time_remind = $time_remind;
        $detail->content_remind = $request->content_remind;
        $detail->save();

        return redirect()->route('work_list',compact(['groupId']));
    }

    //Hàm chuyển sang view edit things to do
    public function editWorkList($groupId, $id)
    {
        $detail = DB::table('details')->where(['id'=>$id])->first();
        return view('workflowmanagement.worklist.editworklist',compact(['id','groupId','detail']));
    }

    //Hàm post form edit things to do
    public function postEditWorkList(RequestEditWorkList $request, $groupId, $id)
    {
        $detail = Detail::find($id);
        $today = Carbon::now('Asia/Ho_Chi_Minh')->toDateTimeString();
        $start_date = $request->start_date ? Carbon::create($request->start_date)->toDateTimeString() : null;
        $finish_date = $request->finish_date ? Carbon::create($request->finish_date)->toDateTimeString() : null;
        $time_remind = $request->time_remind ? Carbon::create($request->time_remind)->toDateTimeString() : null;
        if ($start_date > $finish_date) {
            $request->session()->flash('error','Start date have to smaller than the finish date !');
            $request->flash();
            return redirect()->back()->withInput();
        }
        if ($time_remind) {

            if ($time_remind > $finish_date || $time_remind < $start_date) {
                $request->session()->flash('error','Your time remind wrong !');
                $request->flash();
                return redirect()->back()->withInput();
            }else{
                $detail->status_time_remind = 1;
            }
        }
        if (!$time_remind && $request->content_remind) {
            $request->session()->flash('error','You have to set up your time remind before enter content remind !');
            $request->flash();
            return redirect()->back()->withInput();
        }
        if ($finish_date && ($finish_date < $today)) {
            $detail->status = 2;
            $detail->status_time_remind = 0;
        }else{
            $detail->status = 1;
        }
        $detail->group_id = $groupId;
        $detail->name = $request->name;
        $detail->description = $request->description;
        $detail->start_date = $start_date;
        $detail->finish_date = $finish_date;
        $detail->time_remind = $time_remind;
        $detail->content_remind = $request->content_remind;
        $detail->save();

        return redirect()->route('work_list',compact(['groupId']));
    }

    //Hàm xóa Things to do 
    public function deleteWorkList($id)
    {
        Detail::find($id)->delete();
        return redirect()->back();
    }
}