<?php

namespace App\Http\Controllers\WorkflowManagement;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\RequestCreateGroup;
use App\Http\Requests\RequestEditGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use App\User;
use App\Models\Group;
use App\Models\Detail;
use Auth;
use File;
use App\Repositories\GroupRepository;
use App\Repositories\Contracts\InterfaceRepository;
use App\Library\FileProcessing;

class GroupOfJobsController extends Controller
{
    protected $groupRepository;
    protected $fileProcessing;

    public function __construct(GroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;
        $this->fileProcessing = new FileProcessing;
    }

    /**
    * Show all groupOfJobs
    *
    * @return view group of jobs
    */
    public function groupOfJobs()
    {
        $groups = $this->groupRepository->getAll();
        return view('workflowmanagement.groupofjobs.groupofjobs', compact(['groups']));
    }

    /**
    * Redirect to view create workgroup
    *
    * @return view create group
    */
    public function createGroup()
    {
        return view('workflowmanagement.groupofjobs.creategroup');
    }

    /**
    * Post form create workgroup
    *
    * @param RequestCreateGroup $request
    * @return mixed
    */
    public function postCreateGroup(RequestCreateGroup $request)
    {
        $userId = Auth::id();
        $groups = $this->groupRepository->find($userId)->get();
        foreach ($groups as $g) {
            if ($g->name == $request->name) {
                $request->session()->flash('error','Name group of jobs already exists !');
                return redirect()->back();
            }
        }
        $avatar = $this->fileProcessing->storeFile($request);
        $request->request->remove('avatar');
        $request->merge([
            // 'avatar' => $avatar,
            'user_id' => $userId,
        ]);
        dd($request->all());
        $this->groupRepository->create($request->all());

        return redirect()->route('group_of_jobs');
    }

    /**
    * Redirect to view edit workgroup
    *
    * @param $id
    * @return view edit group
    */
    public function editGroup($id)
    {
        $group = DB::table('groups')->where(['id'=>$id])->first();
        return view('workflowmanagement.groupofjobs.editgroup',compact(['id','group']));
    }

    /**
    * Post form edit workgroup
    *
    * @param RequestCreateGroup $request
    * @param $id
    * @return mixed
    */
    public function postEditGroup(RequestEditGroup $request, $id)
    {
        $groups = DB::table('groups')->where([
            ['id','<>',$id],
            ['user_id','=',Auth::id()]
        ])->get();
        foreach ($groups as $g) {
            if ($g->name == $request->name) {
                $request->session()->flash('error','Name group of jobs already exists !');
                return redirect()->back();
            }
        }
        $group = Group::find($id);
        $group->name = $request->name;
        if ($request->hasFile('avatar')) {
            $fileName = time().'-'.$request->file('avatar')->getClientOriginalName();
            $imgPath = public_path('/img/avatar');
            $imgDeletePath = '/img/avatar'.$group->name;

            if (file_exists($imgDeletePath)) {
                unlink($imgDeletePath);
                $request->file('avatar')->move($imgPath, $fileName);
                $group->avatar = $fileName;
            }else{
                $request->file('avatar')->move($imgPath, $fileName);
                $group->avatar = $fileName;
            }
        }
        $group->save();
        
        return redirect()->route('group_of_jobs');
    }

    /**
    * Delete workgroup
    *
    * @param $id
    * @return mixed
    */ 
    public function deleteGroup($id)
    {
        $group = Group::find($id);
        $imgDeletePath = 'img/avatar'.$group->avatar;
        if (file_exists($imgDeletePath) && $group->avatar != 'anh_demo.jpg') {
            unlink($imgDeletePath);
        }
        $group->delete();
        return redirect()->back();
    }
}