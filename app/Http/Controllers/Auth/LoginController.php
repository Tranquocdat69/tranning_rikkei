<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RequestLogin;
use App\Models\Wallet;
use Auth;

class LoginController extends Controller
{
    //Hàm chuyển sang view login
    public function login()
    {
        return view('auth.login');
    }

    //Hàm post form login
    public function postLogin(RequestLogin $request)
    { 
        if (Auth::attempt($request->only('email', 'password'), $request->has('remember'))) {
            if (Auth::user()->status == 2) {
                return redirect()->route('change_pass_after_reset');
            }
            if (Auth::user()->status == 0) {
                return redirect()->route('add-wallet-for-new-user');
            }
            $first_wallet_id = Wallet::where('user_id', Auth::id())->first('id');
            return redirect()->route('transactions', ['wallet_id' => $first_wallet_id]);
        } else {
            return redirect()->back()->with('error', 'Email or Password incorrect !');
        }
    }
}
