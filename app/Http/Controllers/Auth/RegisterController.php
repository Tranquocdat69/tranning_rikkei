<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\RequestRegister;
use App\Repositories\UserRepository;
use App\User;
use Auth;

class RegisterController extends Controller
{
    protected $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register()
    {
        return view('auth.register');
    }

    public function postRegister(RequestRegister $rq)
    {
        $this->userRepository->registerUser($rq);
        return redirect()->route('login')->with('success', 'Register an account successfully. Let\'s login !!!');
    }
}
