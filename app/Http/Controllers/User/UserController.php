<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestChangePassword;
use App\Http\Requests\RequestEditProfile;
use App\Models\Wallet;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userRepository;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    public function profile()
    {
        $user = $this->userRepository->find(Auth::id());
        return view('user.profile', compact(['user']));
    }

    public function editProfile()
    {
        $user = $this->userRepository->find(Auth::id());
        return view('user.editprofile', compact(['user']));
    }

    public function postEditProfile(RequestEditProfile $rq)
    {
        $rq->offsetUnset('_token');
        $user = User::where('id', '<>', Auth::id())->get();
        foreach ($user as $u) {
            if ($rq->name == $u->name) {
                $rq->session()->flash('error', 'Name already exists');
                return redirect()->back();
            }
            if ($rq->email == $u->email) {
                $rq->session()->flash('error', 'Email address already exists');
                return redirect()->back();
            }
            if ($rq->phone_number == $u->phone_number && $rq->phone_number != null) {
                $rq->session()->flash('error', 'Phone number already exists');
                return redirect()->back();
            }
        }
        User::where(['id' => Auth::id()])->update($rq->all());
        return redirect()->route('profile');
    }

    public function changePass()
    {
        $user = $this->userRepository->find(Auth::id());
        return view('user.changepass', compact(['user']));
    }

    public function postChangePass(RequestChangePassword $rq)
    {
        $id = Auth::id();
        $user = User::find($id);
        if (Hash::check($rq->old_password, $user->password) == false) {
            $rq->session()->flash('error', 'Password does not match with your old password');
            return redirect()->back();
        }
        if ($rq->old_password == $rq->password) {
            $rq->session()->flash('error', 'The new password must be different from the current password');
            return redirect()->back();
        }

        $password = bcrypt($rq->password);
        $rq->merge(['password' => $password]);
        User::where(['id' => $id])->update($rq->only('password'));
        $rq->session()->flash('success', 'Password changed !!!');

        return redirect()->route('profile');
    }

    public function changePassAfterReset()
    {
        $user = $this->userRepository->find(Auth::id());
        return view('user.changepassafterreset', compact(['user']));
    }

    public function postchangePassAfterReset(RequestChangePassword $rq)
    {
        $rq->offsetUnset('_token');
        $id = Auth::id();
        $user = User::find($id); 
        if (Hash::check($rq->old_password, $user->password) == false) {
            $rq->session()->flash('error', 'Password does not match with your old password');
            return redirect()->back(); 
        }
        if ($rq->old_password == $rq->password) {
            $rq->session()->flash('error', 'The new password must be different from the current password');
            return redirect()->back();
        }

        $password = bcrypt($rq->password);
        $rq->merge(['password' => $password]);
        $countWallet = Wallet::where('user_id', $id)->count();
        if ($countWallet > 0) {
            $status = 1;
            User::where(['id' => $id])->update(['password' => $rq->password], ['status' => $status]);
        }
        if ($countWallet < 1) {
            $status = 0;
            User::where(['id' => $id])->update(['password' => $rq->password], ['status' => $status]);
            return redirect()->route('add-wallet-for-new-user');
        }
        $rq->session()->flash('success', 'Password changed !!!');
        $first_wallet_id = Wallet::where('user_id', $id)->first('id');

        return redirect()->route('transactions', ['wallet_id' => $first_wallet_id]);
    }
}
