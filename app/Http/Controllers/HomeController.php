<?php

namespace App\Http\Controllers;

use App\Charts\DashboardChart;
use App\Models\Wallet;
use App\Repositories\WalletRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $walletRepository;

    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }
    public function index()
    {
        $chart = new DashboardChart();
        $chart->labels(['One', 'Two', 'Three','Four']);
        $chart->dataset('My dataset 1', 'pie', [10, 2, 3, 4]);
        $wallets = $this->walletRepository->joinWalletAndImage(Auth::id());
        return view('home', compact(['wallets']));
    }
}
