<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|min:8|max:50',
            'password' => 'required|min:8|max:50',
            'confirm_password' => 'required|min:8|max:50|same:password',
        ];
    }

    public function messages()
    {
        return [
            'confirm_password.same' => 'Confirm password and new password not match'
        ];
    }
    
}
