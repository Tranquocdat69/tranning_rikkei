<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAddTransaction extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'money' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Category can not be blank',
            'money.required' => 'Amount can not be blank'
        ];
    }
}
