<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAddWallet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;  
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'name' => 'required|max:255',
            'amount_of_money' => 'required|integer|max:999999999'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name cannot be blank',
            'amount_of_money.required' => 'Initial Balance cannot be blank',
            'amount_of_money.max' => 'Max is 999,999,999 VNĐ',
            'name.max' => 'Max length of name is 255 characters'
        ];
    }
}
